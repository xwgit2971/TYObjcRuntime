Pod::Spec.new do |s|
  s.name = 'TYObjcRuntime'
  s.version = '0.0.1'
  s.platform = :ios, '8.0'
  s.license = { type: 'MIT', file: 'LICENSE' }
  s.summary = 'Method Swizzle'
  s.homepage = 'https://gitlab.com/xwgit2971/TYObjcRuntime'
  s.author = { 'SunnyX' => '1031787148@qq.com' }
  s.source = { :git => 'git@gitlab.com:xwgit2971/TYObjcRuntime.git', :tag => s.version }
  s.source_files = 'TYObjcRuntime/*.{h,m}'
  s.framework = 'Foundation'
  s.requires_arc = true
end
