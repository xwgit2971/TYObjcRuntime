# TYObjcRuntime
Method Swizzle

## How To Use

```objective-c
#import <AGJObjcRuntime/AGJObjcRuntime.h>
...
+ (void)load {
swizzleAllViewController();
}

void swizzleAllViewController() {
AGJSwizzle([UIViewController class], @selector(viewDidAppear:), @selector(customViewDidAppear:));
AGJSwizzle([UIViewController class], @selector(viewWillDisappear:), @selector(customViewWillDisappear:));
AGJSwizzle([UIViewController class], @selector(viewWillAppear:), @selector(customviewWillAppear:));
}
```
